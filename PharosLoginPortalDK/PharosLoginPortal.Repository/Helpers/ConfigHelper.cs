﻿using System.Configuration;

namespace PharosLoginPortal.Repository.Helpers
{
    public static class ConfigHelper
    {
        public static string GetTokenWebServiceUrl()
        {
            return ConfigurationManager.AppSettings["PharosTokenServiceUrl"];
        }

        public static string GetDbConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }
    }
}
