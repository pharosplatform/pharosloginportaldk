﻿using System;

namespace PharosLoginPortal.Repository.Models
{

    public class Login
    {

        public string HSAIdentity { get; set; }
        public string UserId { get; set; }
        public int Id { get; set; }
        public int LoginId { get; set; }
        public string LoginType { get; set; } //Char i databasen
        public Boolean LoginStatus { get; set; }
        public int RegisterId { get; set; }
        public string RegisterName { get; set; }
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public string CountyName { get; set; }
        public string CommunityName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }

        public string LoginUrl { get; set; }



    }
}