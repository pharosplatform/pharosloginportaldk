﻿namespace PharosLoginPortal.Repository.Models
{
    public class EventLogModel
    {
        public int Id { get; set; }
        public int Eventdatetime { get; set; }
        public int EventNr { get; set; }
        public int EventType { get; set; }
        public int EventCount { get; set; }
        public string UserId { get; set; }
        public string IP { get; set; }
        public string EventMessage { get; set; }
    }
}
