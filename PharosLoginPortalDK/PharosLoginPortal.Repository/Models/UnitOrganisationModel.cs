﻿namespace PharosLoginPortal.Repository.Models
{
    public class UnitOrganisation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UnitId { get; set; }
        public string CommunityName { get; set; }
        public int OrganisationId { get; set; }

        public class CountyList
        {
            public int CountyCode { get; set; }
            public string CountyName { get; set; }
        }

        public class CommunityList
        {
            public int CommunityCode { get; set; }
            public string CommunityName { get; set; }
        }



    }
}
