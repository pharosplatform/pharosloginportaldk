﻿using System.Collections.Generic;

namespace PharosLoginPortal.Repository.Models
{
    public class UserIndexViewModel
    {
        public UserIndexViewModel()
        {
            Logins = new List<Login>();
            LoginRequests = new List<LoginRequests>();
            UserId = 0;
            LoginId = 0;
            HsaId = string.Empty;
        }

        public int UserId { get; set; }
        public int LoginId { get; set; }
        public string HsaId { get; set; }
        public List<Login> Logins { get; set; }
        public List<LoginRequests> LoginRequests { get; set; }
        
    }
}
