﻿using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace PharosLoginPortal.Repository.Models
{
    public class UserNewLoginIndexViewModel
    {

        public UserNewLoginIndexViewModel()
        {
            Registers = new List<Register>();
            UnitOrganisations = new List<UnitOrganisation>();
            UnitCountys = new List<UnitOrganisation.CountyList>();
            UnitCommunitys = new List<UnitOrganisation.CommunityList>();

        }

        public List<Register> Registers { get; set; }
        public List<UnitOrganisation> UnitOrganisations { get; set; }
        public List<UnitOrganisation.CountyList> UnitCountys { get; set; }
        public List<UnitOrganisation.CommunityList> UnitCommunitys { get; set; }
        public IEnumerable<PersonalLoginExistModel> PersonalLoginExist { get; set; } 

    }
}
