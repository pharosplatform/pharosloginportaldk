﻿namespace PharosLoginPortal.Repository.Models
{
    public class Register
    {
        public int Id { get; set; }
        public string RegisterName { get; set; }
        public string PlatformType { get; set; }
        public string RegisterURL { get; set; }
        public string AdminURL { get; set; }
        public string RegisterServer { get; set; }
        public string RegisterDatabase { get; set; }
        public string AdminServer { get; set; }
        public string AdminDatabase { get; set; }
        public string AdminEmailAddress1 { get; set; }
        public string AdminEmailAddress2 { get; set; }
        public bool AlwaysConnectrPersonalLogin { get; set; }
        public int UnitSelectionTpe { get; set; }
        public bool Active { get; set; }
        public string SenderEmailAddress { get; set; }
    }
}
