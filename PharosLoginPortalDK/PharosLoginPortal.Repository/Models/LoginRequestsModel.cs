﻿using System;

namespace PharosLoginPortal.Repository.Models
{

    public class LoginRequests
    {

        public int RegisterId { get; set; }
        public string RegisterName { get; set; }
        public string UnitName { get; set; }   
        public string CommunityName { get; set; }
        public string CountyName { get; set; }
        public string AdminComment { get; set; }
        public string UserComment { get; set; }
        public DateTime CreateDate { get; set; }


    }
}