﻿namespace PharosLoginPortal.Repository.Models
{
    public class NewLoginFormModel 
    {
        public string Register { get; set; }
        public string Unit { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; } //Endast för validering
        public string UserComment { get; set; }
       // public Boolean UserNowQ { get; set; } // JA/NEJ på om man har befintlig inloggning
        public string Username { get; set; }
        public string Password { get; set; }

    }
}
