﻿namespace PharosLoginPortal.Repository.Models
{
    public class PersonalLoginExistModel
    {
        public int UserId { get; set; }
        public int Username { get; set; }
        public string Password { get; set; }
        
    }
}
