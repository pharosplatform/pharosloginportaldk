﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharosLoginPortal.Repository.Models
{
    public class PersonalLoginModel
    {
        public int? UserId { get; set; }
        public int ReturnValue { get; set; }
    }
}
