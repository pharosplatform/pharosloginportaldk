﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using PharosLoginPortal.Repository.Models;

namespace PharosLoginPortal.Repository.Database
{
    public class UnitOrganisationRepository
    {
        private RegisterRepository registerRepository;

        public UnitOrganisationRepository()
        {
            registerRepository = new RegisterRepository();
        }

        public IEnumerable<UnitOrganisation> GetOrganisationAndUnits(int registerId)
        {
            var error = new ErrorLoggingRepository();

            if (registerId > 0)
            {
                var register = registerRepository.GetRegister(registerId);
                if (register != null)
                {
                    using (var connection = new SqlConnection("server=" + register.RegisterServer + "; database=" + register.RegisterDatabase +
                                            ";uid=UPharosLoginPortal;password=PPLP2014!;"))
                    {
                        try
                        {
                            connection.Open();
                            var data = connection.Query<UnitOrganisation>("Pharos_GetOrganisationsAndUnits", param: new
                            {

                            },
                                commandType: CommandType.StoredProcedure);

                            return data;
                        }
                        catch (Exception e)
                        {
                            var exception = e.ToString();
                            var stacktrace = e.StackTrace;
                            error.LoggToDatabase("Unable to perform Stored procedure GetOrganisationAndUnits ", exception, stacktrace);

                        }
                        finally
                        {
                            connection.Dispose();
                        }
                    }
                }
            }

            return null;
        }

        public IEnumerable<UnitOrganisation> GetOrganisationAndUnitsOnCountyAndCommunity(int registerId, int countyId, int communityId)
        {
            var error = new ErrorLoggingRepository();

            if (registerId > 0)
            {
                var register = registerRepository.GetRegister(registerId);
                if (register != null)
                {

                    using (

                        var connection =
                            new SqlConnection("server=" + register.RegisterServer + "; database=" + register.RegisterDatabase +
                                              ";uid=UPharosLoginPortal;password=PPLP2014!;"))
                    {
                        try
                        {
                            connection.Open();
                            var data = connection.Query<UnitOrganisation>("Pharos_GetOrganisationsAndUnits", param: new
                            {
                                LansKod = countyId,
                                kommunKod = communityId
                            },
                                commandType: CommandType.StoredProcedure);

                            return data;
                        }
                        catch (Exception e)
                        {
                            var exception = e.ToString();
                            var stacktrace = e.StackTrace;
                            error.LoggToDatabase(
                                "Unable to perform Stored procedure GetOrganisationAndUnitsOnCountyAndCommunity",
                                exception, stacktrace);

                        }
                        finally
                        {
                            connection.Dispose();
                        }
                    }
                }
            }

            return null;
        }

        public IEnumerable<UnitOrganisation.CountyList> GetOrganisationAndUnitsCountyList(int registerId)
        {
            var error = new ErrorLoggingRepository();

            if (registerId > 0)
            {
                var register = registerRepository.GetRegister(registerId);
                if (register != null)
                {

                    using (

                        var connection =
                            new SqlConnection("server=" + register.RegisterServer + "; database=" + register.RegisterDatabase +
                                              ";uid=UPharosLoginPortal;password=PPLP2014!;"))
                    {
                        try
                        {
                            connection.Open();
                            var data =
                                connection.Query<UnitOrganisation.CountyList>(
                                    "Pharos_GetOrganisationsAndUnitsCountyList", param: new
                                    {
                                    },
                                    commandType: CommandType.StoredProcedure);

                            return data;
                        }
                        catch (Exception e)
                        {
                            var exception = e.ToString();
                            var stacktrace = e.StackTrace;
                            error.LoggToDatabase(
                                "Unable to perform Stored procedure GetOrganisationAndUnitsCountyList", exception,
                                stacktrace);

                        }
                        finally
                        {
                            connection.Dispose();
                        }
                    }
                }

            }

            return null;
        }

        public IEnumerable<UnitOrganisation.CommunityList> GetOrganisationsAndUnitsCommunityList(int registerId, int countyId)
        {
            var error = new ErrorLoggingRepository();

            if (registerId > 0)
            {
                var register = registerRepository.GetRegister(registerId);
                if (register != null)
                {

                    using (

                        var connection =
                            new SqlConnection("server=" + register.RegisterServer + "; database=" + register.RegisterDatabase +
                                              ";uid=UPharosLoginPortal;password=PPLP2014!;"))
                    {
                        try
                        {
                            connection.Open();
                            var data =
                                connection.Query<UnitOrganisation.CommunityList>(
                                    "Pharos_GetOrganisationsAndUnitsCommunityList", param: new
                                    {
                                        LansKod = countyId
                                    },
                                    commandType: CommandType.StoredProcedure);

                            return data;
                        }
                        catch (Exception e)
                        {
                            var exception = e.ToString();
                            var stacktrace = e.StackTrace;
                            error.LoggToDatabase(
                                "Unable to perform Stored procedure GetOrganisationsAndUnitsCommunityList", exception,
                                stacktrace);

                        }
                        finally
                        {
                            connection.Dispose();
                        }
                    }
                }
            }

            return null;
        }

        public UnitOrganisation GetUnit(int unitId, int registerId)
        {
            var error = new ErrorLoggingRepository();

            if (registerId > 0)
            {
                var register = registerRepository.GetRegister(registerId);
                if (register != null)
                {
                    using (var connection = new SqlConnection("server=" + register.RegisterServer + "; database=" + register.RegisterDatabase +
                                            ";uid=UPharosLoginPortal;password=PPLP2014!;"))
                    {
                        try
                        {
                            connection.Open();

                            UnitOrganisation data = null;

                            //Check if SP 'GetUnitById' exists in remote db. If not use Pharos_GetOrganisationsAndUnits to get all units in register and filter afterwards
                            if (connection.Query<string>(
                                    "SELECT ROUTINE_NAME FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'GetUnitById'")
                                    .Any())
                            {
                                data =
                                    connection.Query<UnitOrganisation>("GetUnitById", new {UnitId = unitId},
                                        commandType: CommandType.StoredProcedure).SingleOrDefault();
                            }
                            else
                            {
                                var units = connection.Query<UnitOrganisation>("Pharos_GetOrganisationsAndUnits",
                                        commandType: CommandType.StoredProcedure).ToList();

                                if (units.Any())
                                {
                                    data = units.SingleOrDefault(u => u.UnitId == unitId.ToString());
                                }
                            }

                            return data;
                        }
                        catch (Exception e)
                        {
                            var exception = e.ToString();
                            var stacktrace = e.StackTrace;
                            error.LoggToDatabase("Unable to run Stored procedure GetUnitById ", exception, stacktrace);

                        }
                        finally
                        {
                            connection.Dispose();
                        }
                    }
                }
            }

            return null;
        }
    }
}
