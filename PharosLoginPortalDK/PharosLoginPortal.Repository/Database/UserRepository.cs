﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using PharosLoginPortal.Repository.Models;

namespace PharosLoginPortal.Repository.Database
{
    public class UserRepository
    {
        private RegisterRepository registerRepository;

        public UserRepository()
        {
            registerRepository = new RegisterRepository();
        }

        public IEnumerable<Login> GetUsersSithsLogins(int registerId, string hsaIdentity)
        {
            if (registerId > 0)
            {
                var register = registerRepository.GetRegister(registerId);
                if (register != null)
                {
                    var error = new ErrorLoggingRepository();
                    using (
                        var connection =
                            new SqlConnection("server=" + register.RegisterServer + "; database=" + register.RegisterDatabase +
                                              ";uid=UPharosLoginPortal;password=PPLP2014!;"))
                    {
                        try
                        {
                            connection.Open();
                            var data = connection.Query<Login>("Pharos_GetUserSITHSLogins", param: new
                            {
                                HSAIdentity = hsaIdentity
                            },

                                commandType: CommandType.StoredProcedure);

                            return data;
                        }
                        catch (Exception e)
                        {
                            var exception = e.ToString();
                            var stacktrace = e.StackTrace;
                            error.LoggToDatabase("Unable to perform Stored procedure Pharos_GetUserSITHSLogins ",
                                exception, stacktrace);
                        }
                        finally
                        {
                            connection.Dispose();
                        }

                    }
                }
            }
            return null;
        }

        public IEnumerable<LoginRequests> GetUsersSithsLoginRequests(int registerId, string hsaIdentity)
        {
            if (registerId > 0)
            {
                var register = registerRepository.GetRegister(registerId);
                if (register != null)
                {
                    var error = new ErrorLoggingRepository();

                    using (
                        var connection =
                            new SqlConnection("server=" + register.RegisterServer + "; database=" + register.RegisterDatabase +
                                              ";uid=UPharosLoginPortal;password=PPLP2014!;"))
                    {
                        try
                        {
                            connection.Open();
                            var data = connection.Query<LoginRequests>("Pharos_GetUsersSITHSLoginRequests", param: new
                            {
                                HSAIdentity = hsaIdentity
                            },

                                commandType: CommandType.StoredProcedure);

                            return data;
                        }
                        catch (Exception e)
                        {
                            var exception = e.ToString();
                            var stacktrace = e.StackTrace;
                            error.LoggToDatabase(
                                "Unable to perform Stored procedure Pharos_GetUsersSITHSLoginRequests ", exception,
                                stacktrace);
                        }

                    }
                }
            }

            return null;
        }

        public PersonalLoginModel DoesPersonalLoginExist(int? unitId, string password, string username, int registerId)
        {
            var personalLogin = new PersonalLoginModel();
            var error = new ErrorLoggingRepository();
            if (registerId > 0)
            {
                var register = registerRepository.GetRegister(registerId);
                if (register != null)
                {
                    using (
                        var connection =
                            new SqlConnection("server=" + register.RegisterServer + "; database=" + register.RegisterDatabase +
                                              ";uid=UPharosLoginPortal;password=PPLP2014!;"))
                    {
                        try
                        {
                            connection.Open();


                            var parameters = new DynamicParameters();
                            parameters.Add("UnitId", unitId);
                            parameters.Add("Username", username);
                            parameters.Add("Password", password);
                            parameters.Add("UserId", SqlDbType.Int, direction: ParameterDirection.InputOutput);
                            parameters.Add("return_value", SqlDbType.Int, direction: ParameterDirection.ReturnValue);
                            var data = connection.Query("[dbo].[Pharos_DoesPersonalLoginExist?]", parameters,
                                commandType: CommandType.StoredProcedure);
                            personalLogin.UserId = parameters.Get<int?>("UserId");
                            personalLogin.ReturnValue = parameters.Get<int>("return_value");



                            return personalLogin;
                        }
                        catch (Exception e)
                        {
                            var exception = e.ToString();
                            var stacktrace = e.StackTrace;
                            error.LoggToDatabase("Unable to perform Stored procedure DoesPersonalLoginExist ", exception,
                                stacktrace);
                        }
                        finally
                        {
                            connection.Dispose();
                        }

                    }
                }
            }


            return null;
        }

        public int? CreateSithsLoginRequest(string hsaIdentity, string firstName, string lastName,
            string email, string userComment, int? userId, int registerId, int? organisationId, int? unitId, int? trainingId,
            int loginRequestId)
        {
            if (registerId > 0)
            {
                var register = registerRepository.GetRegister(registerId);
                if (register != null)
                {
                    var error = new ErrorLoggingRepository();
                    using (
                        var connection =
                            new SqlConnection("server=" + register.RegisterServer + "; database=" + register.RegisterDatabase +
                                              ";uid=UPharosLoginPortal;password=PPLP2014!;"))
                    {
                        try
                        {
                            connection.Open();

                            var parameters = new DynamicParameters();
                            parameters.Add("HSAIdentity", hsaIdentity);
                            parameters.Add("FirstName", firstName);
                            parameters.Add("LastName", lastName);
                            parameters.Add("EmailAddress", email);
                            parameters.Add("UserComment", userComment);
                            parameters.Add("UserId", userId);
                            parameters.Add("RegisterId", registerId);
                            parameters.Add("OrganisationId", organisationId);
                            parameters.Add("TrainingId", trainingId);
                            parameters.Add("UnitId", unitId);
                            parameters.Add("LoginRequestId", loginRequestId);
                            parameters.Add("return_value", SqlDbType.Int, direction: ParameterDirection.ReturnValue);
                            var data = connection.Query("[dbo].[Pharos_CreateSITHSLoginRequest]", parameters,
                                commandType: CommandType.StoredProcedure);

                            var result = parameters.Get<int>("return_value");

                            return result;
                        }
                        catch (Exception e)
                        {
                            var exception = e.ToString();
                            var stacktrace = e.StackTrace;
                            error.LoggToDatabase("Unable to perform Stored procedure CreateSithsLoginRequest ",
                                exception, stacktrace);
                        }


                    }
                }
            }
            const int errorInt = -5;
            return errorInt;
        }


        public int DoesPendingSithsLoginRequestExist(int registerId, string hsaIdentity, int? organisationId, int? unitId)
        {
            if (registerId > 0)
            {
                var register = registerRepository.GetRegister(registerId);
                if (register != null)
                {
                    var error = new ErrorLoggingRepository();

                    using (
                        var connection =
                            new SqlConnection("server=" + register.RegisterServer + "; database=" + register.RegisterDatabase +
                                              ";uid=UPharosLoginPortal;password=PPLP2014!;"))
                    {
                        try
                        {
                            connection.Open();
                            var parameters = new DynamicParameters();
                            parameters.Add("HsaIdentity", hsaIdentity);
                            parameters.Add("OrganisationId", organisationId);
                            parameters.Add("UnitId", unitId);
                            parameters.Add("return_value", SqlDbType.Int, direction: ParameterDirection.ReturnValue);
                            var data = connection.Query("[dbo].[Pharos_DoesPendingSITHSLoginRequestExist?]", parameters,
                                commandType: CommandType.StoredProcedure);

                            var result = parameters.Get<int>("return_value");

                            return result;
                        }
                        catch (Exception e)
                        {
                            var exception = e.ToString();
                            var stacktrace = e.StackTrace;
                            error.LoggToDatabase(
                                "Unable to perform Stored procedure DoesPendingSithsLoginRequestExist ", exception,
                                stacktrace);
                        }
                        finally
                        {
                            connection.Dispose();
                        }


                    }
                }
            }
            const int errorInt = 5;
            return errorInt;
        }

        public int DoesSithsLoginExist(int registerId, string hsaIdentity, int? organisationId, int? unitId)
        {
            if (registerId > 0)
            {
                var register = registerRepository.GetRegister(registerId);
                if (register != null)
                {
                    var error = new ErrorLoggingRepository();

                    using (
                        var connection =
                            new SqlConnection("server=" + register.RegisterServer + "; database=" + register.RegisterDatabase +
                                              ";uid=UPharosLoginPortal;password=PPLP2014!;"))
                    {
                        try
                        {
                            connection.Open();
                            var parameters = new DynamicParameters();
                            parameters.Add("HsaIdentity", hsaIdentity);
                            parameters.Add("OrganisationId", organisationId);
                            parameters.Add("UnitId", unitId);
                            parameters.Add("return_value", SqlDbType.Int, direction: ParameterDirection.ReturnValue);
                            var data = connection.Query("[dbo].[Pharos_DoesSITHSLoginExist?]", parameters,
                                commandType: CommandType.StoredProcedure);

                            var result = parameters.Get<int>("return_value");

                            return result;

                        }
                        catch (Exception e)
                        {
                            var exception = e.ToString();
                            var stacktrace = e.StackTrace;
                            error.LoggToDatabase("Unable to perform Stored procedure DoesSithsLoginExist ", exception,
                                stacktrace);

                        }
                        finally
                        {
                            connection.Dispose();
                        }


                    }
                }
            }
            var errorInt = 5;
            return errorInt;

        }

    }
}


















