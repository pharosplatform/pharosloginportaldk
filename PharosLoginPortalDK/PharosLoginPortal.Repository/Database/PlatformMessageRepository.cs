﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using System.Threading.Tasks;
using PharosLoginPortal.Repository.Helpers;

namespace PharosLoginPortal.Repository.Database
{
    public class PlatformMessageRepository
    {

        public string GetPlatformMessage()
        {

            using (var connection = new SqlConnection(ConfigHelper.GetDbConnectionString()))
            {
                var error = new ErrorLoggingRepository();
                try
                {
                    connection.Open();
                    var msg = connection.Query<string>("select PlatformMessage from PLP_PlatformMessage").ElementAt(0);
                    connection.Close();
                    return msg;

                }
                catch (Exception e)
                {
                    var exception = e.ToString();
                    var stacktrace = e.StackTrace;
                    error.LoggToDatabase("Unable to retrieve platform message from db.PlatformMessage", exception, stacktrace);
                    return string.Empty;
                }
                finally
                {
                    connection.Dispose();
                }


            }
            return null;
        }

    }
}
