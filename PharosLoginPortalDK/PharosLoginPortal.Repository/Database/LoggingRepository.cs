﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using PharosLoginPortal.Repository.Helpers;

namespace PharosLoginPortal.Repository.Database
{
    public class LoggingRepository
    {
        protected string GetIpAdress ()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            var ipAdress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAdress))
            {
                string[] addresses = ipAdress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }

            }

            return context.Request.ServerVariables["REMOTE_ADDR"];

        }
        public void LoggToDatabase( int eventType, int eventNumber, int userId, string eventMessage)
        {

            string ipAdress = GetIpAdress();
            DateTime date = DateTime.Now;
            int eventCount = 1;

                    
            using (var connection = new SqlConnection(ConfigHelper.GetDbConnectionString()))
            {
                var error = new ErrorLoggingRepository();
                try
                {
                    connection.Open();
                    string sqlQuery =
                        "INSERT INTO [dbo].[EventLog]([Eventdatetime],[EventNr],[EventType],[EventCount],[UserId],[IP],[EventMessage]) " +
                        "VALUES (@Eventdatetime, @EventNr, @EventType, @EventCount,  @UserId,  @IP,  @EventMessage  )";
                    connection.Execute(sqlQuery, new
                    {
                        Eventdatetime = date,
                        EventNr = eventNumber,
                        EventType = eventType,
                        EventCount = eventCount,
                        UserId = userId,
                        IP = ipAdress,
                        EventMessage = eventMessage,

                    });
                    connection.Close();

                }
                catch (Exception e)
                {
                    var exception = e.ToString();
                    var stacktrace = e.StackTrace;
                    error.LoggToDatabase("Unable to Logg To dbo.EventLog", exception, stacktrace);
                }
                finally
                {
                    connection.Dispose();
                }


            }
        }


    }
}