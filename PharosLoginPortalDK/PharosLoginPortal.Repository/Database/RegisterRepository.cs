﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using PharosLoginPortal.Repository.Helpers;
using PharosLoginPortal.Repository.Models;

namespace PharosLoginPortal.Repository.Database
{
    public class RegisterRepository
    {

        public IEnumerable<Register> GetAllRegisters()
        {
          
            using (var connection = new SqlConnection(ConfigHelper.GetDbConnectionString()))
            {
                var error = new ErrorLoggingRepository();
                try
                {
                    connection.Open();
                    var reg = connection.Query<Register>("select * from Registers where Active=1;");
                    connection.Close();
                    return reg;

                }
                catch (Exception e)
                {
                    var exception = e.ToString();
                    var stacktrace = e.StackTrace;
                    error.LoggToDatabase("Unable to retrieve data from db.Registers", exception, stacktrace);
                }
                finally
                {
                    connection.Dispose();
                }


            }
            return null;
        }

        public Register GetRegister(int id)
        {
            using (var connection = new SqlConnection(ConfigHelper.GetDbConnectionString()))
            {
                var error = new ErrorLoggingRepository();
                try
                {
                    connection.Open();
                    var reg = connection.Query<Register>("SELECT * FROM Registers WHERE Id = @RegisterId AND Active = 1;", new { RegisterId = id }).SingleOrDefault();
                    connection.Close();
                    return reg;

                }
                catch (Exception e)
                {
                    var exception = e.ToString();
                    var stacktrace = e.StackTrace;
                    error.LoggToDatabase("Unable to retrieve data from db.Registers", exception, stacktrace);
                }
                finally
                {
                    connection.Dispose();
                }


            }
            return null;
        }
    }
}
