﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using Dapper;
using PharosLoginPortal.Repository.Helpers;

namespace PharosLoginPortal.Repository.Database
{
    public class ErrorLoggingRepository
    {
        protected string GetIpAdress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            var ipAdress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAdress))
            {
                string[] addresses = ipAdress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }

            }

            return context.Request.ServerVariables["REMOTE_ADDR"];

        }
        public void LoggToDatabase(string message, string exception, string stacktrace)
        {

            var machine = new CallerMemberNameAttribute();

            DateTime date = DateTime.Now;
            string application = "Pharos Login Portal";
            string level = "";
            string logger = "";
            string machinename = GetIpAdress();
            string username = "test";
            string callsite = "";
            string thread = "";
             
            using (var connection = new SqlConnection(ConfigHelper.GetDbConnectionString()))
            {

                try
                {
                    connection.Open();
                    string sqlQuery = "INSERT INTO [dbo].[ErrorLog]([LoggedDateTime],[Application],[Level],[Logger],[Message],[MachineName],[UserName],[CallSite],[Thread],[Exception],[Stacktrace]) " +
                                                            "VALUES (@LoggedDateTime, @Application, @Level, @Logger,  @Message,  @MachineName,  @UserName, @CallSite, @Thread, @Exception,@Stacktrace  )";
                    connection.Execute(sqlQuery, new
                    {
                        LoggedDateTime = date,
                        Application = application,
                        Level = level,
                        Logger = logger,
                        Message = message,
                        MachineName = machinename,
                        UserName = username,
                        CallSite = callsite,
                        Thread = thread,
                        Exception = exception,
                        Stacktrace = stacktrace

                    });
                    connection.Close();

                }
                catch (Exception)
                {
                    
                }

            }
        }
    }
}
