﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AuthClient_asp._Default" %>
<%@ Import Namespace="AuthClient_asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Authify Client Demo</title>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
</head>
<body>

<%    
/**
 * Setup "credentials" authifyClientRest with your "Application ID and Secret"
 * This will automatically pull in any parameters,to validate them against the
 * session signature.
 * @param   your_domain     your domain.
 * 
 * @author     Authify Team.
 * @version    1.7
 *
 * http://www.authify.com/
 */
    
	
    AuthifyClientRest aCnet =
        new AuthifyClientRest(
            authify_include_keys.AUTHIFY_APPID,
            authify_include_keys.AUTHIFY_SECRETKEY,
            //"http://your_domain/landing_back_page.aspx"
           "http://" + Request.ServerVariables["SERVER_NAME"] + Request.ServerVariables["URL"]

        );

    if (AuthifyClientRest.IsServerAuthenticated())
    {
        if (AuthifyClientRest.GetProperties("state", (string)Request["authify_response_token"]) == "login")
        {        
            if ((string)Request["idp"] != null) //TOFIX:
            {
                Response.Redirect(AuthifyClientRest.RequireLogin((string)Request["idp"]));
            }

            %>You are not logged in.<br /><br />Login with:<br/><%

            string idp = AuthifyClientRest.GetProperties("item", (string)Request["authify_response_token"]);
            string[] idpx = idp.Split(',');

            for (int i = 0; i < idpx.Length; i++)
            {
                %><a href="?idp=<%=idpx[i]%>"><%=idpx[i]%></a><br/><%
            }        
        }

        if (Request["logout"] != null) //TOFIX:
        {
            AuthifyClientRest.RequireLogout((string)Request["authify_response_token"]);
            Response.Redirect("Default.aspx");
        } 

        if (AuthifyClientRest.GetProperties("state", (string)Request["authify_response_token"]) == "logout")
        {
            %>You are logged in.<br/>
        
            <a href="?logout=logout&authify_response_token=<%=Request["authify_response_token"]%>">Logout</a><br /><%
           Response.Write(AuthifyClientRest.GetResponse("json", (string)Request["authify_response_token"]));                     
        }
    }else
        { 
            %>Security Error: Server was not Authenticated<br /><%
        }
%>
</body>
</html>


