﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;

namespace AuthClient_asp
{
    public class AuthifyClientRest
    {
        private static string _callbackurl;
        private static string _apiKey;
        private static string _secretKey;

        public static string[] AuthifyServers =    {
            "https://loginserver1.authify.com/",
            "https://loginserver1.authify.com/",
            "https://loginserver1.authify.com/",
            "https://loginserver1.authify.com/",
            "https://loginserver1.authify.com/"
        };
        public static int ServerUp = 0;
        private static string _service;
        private static string _resellerId = "8953565";
        private const string V = "8.5";

        public static void SetupClient(string apiKey, string secretKey, string callbackurl)
        {
            _callbackurl = callbackurl;
            _apiKey = apiKey;
            _secretKey = secretKey;
            _service = "require_login";
            _resellerId = "8953565";
        }

        public static string RequireLogin(string idp)
        {
            return RequireLogin(idp, "", "");
        }

        public static string GetAuthifyUniqueHash()
        {
            var guidResult = string.Empty;
            while (guidResult.Length < 60)
            {
                // Get the GUID.
                guidResult += Guid.NewGuid().ToString().GetHashCode().ToString("x");
            }

            // Return the first length bytes.
            return guidResult.Substring(0, 60);
        }


        public static string RequireLogin(string idp, string loginparameterx, string localUserId)
        {
            var authifyRequestTokenHash = GetAuthifyUniqueHash();
            var openWith = new Dictionary<string, string>
                {
                    {"api_key", _apiKey},
                    {"uri", _callbackurl},
                    {"secret_key", _secretKey},
                    {"authify_request_token", authifyRequestTokenHash},
                    {"idp", idp},
                    {"luid", localUserId},
                    {"loginparameters", loginparameterx},
                    {"function", _service},
                    {"reseller_id", _resellerId},
                    {"v", V}
                };

            authifyrest_GetRequestStream_request_post(AuthifyServers[ServerUp] + "request/", openWith, "POST");
            return AuthifyServers[ServerUp] + "tokenidx.php?authify_request_token=" + authifyRequestTokenHash;
        }

        public static string RequireLogout(string authifyResponseToken)
        {
            var openWith =
                new Dictionary<string, string> {{"authify_checksum", authifyResponseToken}, {"v", V}};

            return authifyrest_GetRequestStream_request_post(AuthifyServers[ServerUp] + "out/", openWith, "POST");
        }

        public static bool SendDataToAuthify(string authifyResponseToken)
        {
            return SendDataToAuthify("", authifyResponseToken);
        }

        public static bool SendDataToAuthify(string xml, string authifyResponseToken)
        {
            var openWith =
                new Dictionary<string, string>
                {
                    {"extradata", xml},
                    {"secret_key", _secretKey},
                    {"api_key", _apiKey},
                    {"authify_reponse_token", authifyResponseToken},
                    {"function", "ExtradataProfiles"},
                    {"v", V}
                };

            return (authifyrest_GetRequestStream_request_post(AuthifyServers[ServerUp] + "store/", openWith, "POST").Length != 0);
        }

        public void SetMapping()
        {
            _service = "require_login_mapping";
        }

        public static string GetProperties(string value, string authifyResponseToken)
        {
            var doc = new XmlDocument {PreserveWhitespace = true};
            doc.LoadXml(GetResponse("soap", authifyResponseToken).TrimStart());

            // Check to see if the element has a @value TagName.
            var nlist = doc.DocumentElement.GetElementsByTagName(value);

            //return @string as value ;
            string valueas;
            try
            {
                valueas = nlist.Item(0).ChildNodes[0].Value;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception thrown {0} {1}", e.GetType(), e.Message);
                valueas = "";
            }
            return valueas;
        }

        public static string GetResponse(string format, string authifyResponseToken)
        {
            var openWith =
                new Dictionary<string, string>
                {
                    {"authify_checksum", authifyResponseToken},
                    {"api_key", _apiKey},
                    {"uri", _callbackurl},
                    {"secret_key", _secretKey},
                    {"protocol", format},
                    {"v", V}
                };

            //soap
            return authifyrest_GetRequestStream_request_post(AuthifyServers[ServerUp] + "json/", openWith, "POST");
        }

        public static string authifyrest_GetRequestStream_request_post(string serverAddr, Dictionary<string, string> parames, string Method)
        {
            // Create a request using a URL that can receive a post. 
            var request = WebRequest.Create(serverAddr);

            // Set the Method property of the request to POST.
            request.Method = Method;

            // Create POST data and convert it to a byte array.
            var pd = new StringBuilder();
            //parames.Add("ip_ad", Request.ServerVariables["REMOTE_ADDR"]);
            foreach (var e in parames)
            {
                if (pd.Length > 0)
                    pd.Append('&');

                pd.Append(e.Key);
                pd.Append('=');
                pd.Append(e.Value);
            }

            var postData = pd.ToString();
            var byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";

            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;

            // Get the request stream.
            var dataStream = request.GetRequestStream();

            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);

            // Close the Stream object.
            dataStream.Close();

            // Get the response.
            WebResponse response = null;
            try
            {
                response = request.GetResponse();
            }
            catch (Exception e)
            {

                Console.WriteLine("Exception thrown {0} {1}", e.GetType(), e.Message);
            }

            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();

            // Open the stream using a StreamReader for easy access.
            var reader = new StreamReader(dataStream);

            // Read the content.
            string responseFromServer = reader.ReadToEnd();

            // Display the content.
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();
            return responseFromServer;
        }

        static bool CertificateValidationCallback(
            object sender,
            X509Certificate certificate,
            X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static bool IsServerAuthenticated()
        {
            try
            {
                const string ourDnsName = "authify.com";
                const string serverName = "loginserver1.authify.com";

                var sslClient = new TcpClient();
                sslClient.Connect(serverName, 443);

                var sslStream =
                    new SslStream(sslClient.GetStream(), false,
                        CertificateValidationCallback);

                sslStream.AuthenticateAsClient(serverName);

                var cert = new X509Certificate2(sslStream.RemoteCertificate);

                return cert.Verify() && cert.GetNameInfo(X509NameType.DnsName, false).Equals(ourDnsName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception thrown {0} {1}", ex.GetType(), ex.Message);
            }

            return false;
        }
    }
}
