﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PharosLoginPortal.Web.Startup))]
namespace PharosLoginPortal.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
