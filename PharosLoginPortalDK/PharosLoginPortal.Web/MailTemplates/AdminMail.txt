﻿Hej,

En ny förfrågan om inloggning till {%RegisterName%} och enheten '{%UnitName%}' har skickats in.

Avsändare: 
{%Name%}
{%HSAIdentity%}

Datum för förfrågan: {%Date%}

Med vänliga hälsningar
{%RegisterName%}