﻿using System;
using System.Collections.Generic;
using System.Linq;
// ReSharper disable InconsistentNaming
namespace PharosLoginPortal.Web.Models
{
    public class AuthifyUserContainer
    {
        public IEnumerable<AuthifyUser> data { get; set; }
    }

    public class AuthifyNemIDUser
    {
        public string hsa_id { get; set; }
        public string name { get; set; }
        public string organization_name { get; set; }
        public string email { get; set; }

        public string SithToken { get; set; }
        public int? trainingId { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string uid { get; set; }
    }

    public class AuthifyUser
    {
        public string item { get; set; }
        public string state { get; set; }
        public string idp { get; set; }
        public string uid { get; set; }
        public string mapuid { get; set; }
        public string luid { get; set; }
        public string idpuid { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string extra { get; set; }
        public string SithToken { get; set; }
        public int? trainingId { get; set; }
        public Dictionary<string, string> ExtraInformationDictionary { get; set; }

        public string hsa_id { get { return ExtraInformationDictionary["hsa_id"]; } }
        public string firstname { get { return ExtraInformationDictionary["firstname"]; } }
        public string lastname { get { return ExtraInformationDictionary["lastname"]; } }
        public string organization_number { get { return ExtraInformationDictionary["organization_number"]; } }
        public string organization_name { get { return ExtraInformationDictionary["organization_name"]; } }
        public string hsa_id_number { get { return ExtraInformationDictionary["hsa_id_number"]; } }
        public string title { get { return ExtraInformationDictionary["title"]; } }
        public string certSerialNumber { get { return ExtraInformationDictionary["certSerialNumber"]; } }

        public void SetupExtraInformationDictionary()
        {
            ExtraInformationDictionary = new Dictionary<string, string>();
            var firstSplit = extra.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var propertyList = new List<string>();

            foreach (var row in firstSplit)
            {
                if (row.Contains(":"))
                    propertyList.Add(row);
                else
                {
                    if (!propertyList.Any())
                        throw new ArgumentException("Unexpected authentication information.");
                    propertyList[propertyList.Count - 1] += " " + row;
                }
            }

            foreach (var property in propertyList)
            {
                if (!property.Contains(':'))
                    throw new ArgumentException("Failed to extract extra properties from authentication. No ':' found in property.");
                var keyAndValue = property.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                switch (keyAndValue.Count())
                {
                    case 2:
                        ExtraInformationDictionary.Add(keyAndValue.First(), keyAndValue.Last());
                        break;
                    case 1:
                        ExtraInformationDictionary.Add(keyAndValue.First(), null);
                        break;
                    default:
                        throw new ArgumentException("Failed to extract extra properties from authentication. Too many ':'.");
                }
            }
        }



        public static AuthifyUser GetFakeLogin()
        {
            var user = new AuthifyUser
            {
                item = "gmail,siths",
                state = "logout",
                idp = "siths",
                uid = "154617",
                mapuid = "154617",
                luid="",
                //idpuid="SE162321000255-177300",
                idpuid = "SE162321000255-103461",
                name ="Richard Bibby",
                email = "Richard.Bibby@skane.se",
                extra = "hsa_id:SE162321000255-103461 name:Richard Bibby firstname:Richard lastname:Bibby organization_number:2321000255 organization_name:Region Skåne email:Richard.Bibby@skane.se title: hsa_id_number: certSerialNumber:83523222174189403190733899210074854232 "
            };
            user.SetupExtraInformationDictionary();
            return user;
        }
    }
}