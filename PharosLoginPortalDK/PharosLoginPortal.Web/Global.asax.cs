﻿using System;
using System.Collections.Generic;
using System.IdentityModel;
using System.IdentityModel.Services;
using System.IdentityModel.Services.Configuration;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using PharosLoginPortal.Web.App_Start;

namespace PharosLoginPortal.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // NemID
           // FederatedAuthentication.FederationConfigurationCreated += FederatedAuthenticationOnFederationConfigurationCreated;
        }

        // NemID
        private void FederatedAuthenticationOnFederationConfigurationCreated(object sender, FederationConfigurationCreatedEventArgs federationConfigurationCreatedEventArgs)
        {
            // Assign the service certificate to encrypt session cookies
            // used in wif. Dynamic keys won't work in an Azure environment where the app 
            // might be switched around
            var sessionTransforms = new
                List<CookieTransform>(new CookieTransform[]
                                          {
                                              new DeflateCookieTransform(),
                                              new RsaEncryptionCookieTransform(federationConfigurationCreatedEventArgs.FederationConfiguration.ServiceCertificate)
                                          });

            var sessionHandler = new SessionSecurityTokenHandler(sessionTransforms.AsReadOnly());

            federationConfigurationCreatedEventArgs.FederationConfiguration.IdentityConfiguration.SecurityTokenHandlers.AddOrReplace(sessionHandler);
        }
    }
}
