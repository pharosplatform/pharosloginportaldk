﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using Newtonsoft.Json;
using PharosLoginPortal.Web.Models;

namespace PharosLoginPortal.Web
{
    public static class SessionExtensions
    {
        public static ClaimsIdentity GetClaimsIdentity(this IPrincipal principal)
        {
            if (principal == null)
                return null;

            var getClaimsIdentity = principal.Identity as ClaimsIdentity;
            return getClaimsIdentity;
        }

        public static AuthifyNemIDUser GetAuthifyUser(this IPrincipal principal)
        {
            var claimsIdentity = principal.GetClaimsIdentity();
            if (claimsIdentity == null)
                return null;

            var userData = new AuthifyNemIDUser();
            var claims = claimsIdentity.Claims;

            foreach (var claim in claims)
            {
                switch (claim.Type)
                {
                    case "urn:oid:2.5.4.3":
                        userData.name = claim.Value;
                        userData.firstname = claim.Value.Substring(0,
                            claim.Value.IndexOf(" ", StringComparison.CurrentCulture));
                        break;
                    case "urn:oid:0.9.2342.19200300.100.1.3":
                        userData.email = claim.Value;
                        break;
                    case "urn:oid:2.5.4.10":
                        userData.organization_name = claim.Value;
                        break;
                    case "dk:gov:saml:attribute:CvrNumberIdentifier":
                        userData.hsa_id = claim.Value;
                        break;
                    case "dk:gov:saml:attribute:UniqueAccountKey":
                        userData.uid = claim.Value;
                        userData.SithToken = claim.Value;
                        break;
                    case "urn:oid:2.5.4.4":
                        userData.lastname = claim.Value;
                        break;
                }
            }

            //var userData = claimsIdentity.Claims.SingleOrDefault(claim => claim.Type.Equals(ClaimTypes.UserData));
            return userData;// == null ? null : JsonConvert.DeserializeObject<AuthifyUser>(userData.Value);
        }
    }
}