﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PharosLoginPortal.Repository.Database;
using PharosLoginPortal.Repository.Models;

namespace PharosLoginPortal.Web.Controllers
{

    [Authorize]
    public class NewLoginController : Controller
    {
        private readonly RegisterRepository _registerRepository;
        private readonly UnitOrganisationRepository _unitOrganisationRepository;
        private readonly UserRepository _userRepository;
        
        public NewLoginController()
        {
            _registerRepository = new RegisterRepository();
            _unitOrganisationRepository = new UnitOrganisationRepository();
            _userRepository = new UserRepository();
        }
      
        public ActionResult Index()
        {
            var myViewModel = new UserNewLoginIndexViewModel();
            var registers = _registerRepository.GetAllRegisters();

            myViewModel.Registers.AddRange(registers);
            return View(myViewModel);
        }
      
        public JsonResult GetOrganisationAndUnits(int id)
        {
            var result = _unitOrganisationRepository.GetOrganisationAndUnits(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrganisationAndUnitsCountyList(int id)
        {
            var result = _unitOrganisationRepository.GetOrganisationAndUnitsCountyList(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    
        public JsonResult GetOrganisationsAndUnitsCommunityList(int id, int countyId)
        {
            var result = _unitOrganisationRepository.GetOrganisationsAndUnitsCommunityList(id, countyId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
     
        public JsonResult GetOrganisationAndUnitsOnCountyAndCommunity(int id, int countyId, int communityId)
        {
            var result = _unitOrganisationRepository.GetOrganisationAndUnitsOnCountyAndCommunity(id, countyId, communityId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    
        public JsonResult DoesPersonalLoginExist(int? unitId, string password, string username, int registerId)
        {
            var logging = new LoggingRepository();
            logging.LoggToDatabase(1, 22, 0, "Submited password and username for personal login");
            var userRepository = new UserRepository();
            var result = userRepository.DoesPersonalLoginExist(unitId, password, username, registerId); 

            return Json(result, JsonRequestBehavior.AllowGet);
        }
   
        public JsonResult CreateSithsLoginRequest(string email, string userComment, int? userId, int registerId, int? organisationId, int? unitId, int loginRequestId)
        {
            var hsaIdentity = User.GetAuthifyUser().hsa_id;
            var firstName = User.GetAuthifyUser().firstname;
            var lastName = User.GetAuthifyUser().lastname;
            var trainingId = User.GetAuthifyUser().trainingId;

            if (userId <= 0)
            {
                userId = null;
            }

            if (organisationId <= 0)
            {
                organisationId = null;
            }

            if (unitId < 0) //unitId can be 0 and still be valid
            {
                unitId = null;
            }

            var logging = new LoggingRepository();
            logging.LoggToDatabase(1, 23, 0, "Submited New Login Request");

            var sithsLoginExist = _userRepository.DoesSithsLoginExist(registerId, hsaIdentity, organisationId, unitId);
            if (sithsLoginExist == 1)
            {
                logging.LoggToDatabase(2, 24, 0, "Siths login exists");

                return Json("Siths login finns redan", JsonRequestBehavior.AllowGet);
            }

            var loginRequestExist = _userRepository.DoesPendingSithsLoginRequestExist(registerId, hsaIdentity,
                organisationId, unitId);
            if (loginRequestExist == 1)
            {
                logging.LoggToDatabase(2, 25, 0, "Request exists");
                return Json("Ansökan finns redan", JsonRequestBehavior.AllowGet);
            }

            logging.LoggToDatabase(1, 26, 0, "Request submited to database");
            var result = _userRepository.CreateSithsLoginRequest(hsaIdentity, firstName, lastName, email, userComment, userId, registerId, organisationId, unitId, trainingId, loginRequestId);

            if (result.HasValue)
            {
                //Send email notifications
                var register = _registerRepository.GetRegister(registerId);
                var unitName = "N/A";
                if (unitId.HasValue)
                {
                    var unit = _unitOrganisationRepository.GetUnit(unitId.Value, registerId);
                    if (unit != null)
                        unitName = unit.Name;
                }

                var senderAddress = !string.IsNullOrWhiteSpace(register.SenderEmailAddress)
                    ? register.SenderEmailAddress
                    : !string.IsNullOrWhiteSpace(register.AdminEmailAddress1) 
                        ? register.AdminEmailAddress1 
                        : register.AdminEmailAddress2;

                var adminEmailAddresses = "";
                adminEmailAddresses += register.AdminEmailAddress1 ?? "";
                adminEmailAddresses += (adminEmailAddresses != "" ? "," : "") + register.AdminEmailAddress2;

                //To the user
                EmailUserConfirmation(hsaIdentity, "New SITHS Login Request created", "Din förfrågan har skickats", senderAddress, email, firstName, register.RegisterName, unitName);

                //To the administrator
                EmailAdminConfirmation(hsaIdentity, "New SITHS Login Request created", "[" + register.RegisterName + "] En ny inloggningsförfrågan har skickats in", senderAddress, adminEmailAddresses, lastName + ", " + firstName, register.RegisterName, unitName);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void EmailUserConfirmation(string hsaIdentity, string description, string emailSubject, string senderAddress, string addressees, string name, string registerName, string unitName)
        {
            var emailBody = FormatEmailBody(hsaIdentity, name, registerName, unitName, "~/MailTemplates/UserMail.txt");
            const string senderName = "Pharos Login Portal";
            const string attachmentName = "";

            var service = new Npat.Eyenet.EmailService.IwsZooshMSservice();

            service.SendEmail(emailSubject, emailSubject, emailBody, senderName, senderAddress, addressees, attachmentName, null);
        }

        public void EmailAdminConfirmation(string hsaIdentity, string description, string emailSubject, string senderAddress, string addressees, string name, string registerName, string unitName)
        {
            var emailBody = FormatEmailBody(hsaIdentity, name, registerName, unitName, "~/MailTemplates/AdminMail.txt");
            const string senderName = "Pharos Login Portal";
            const string attachmentName = "";

            var service = new Npat.Eyenet.EmailService.IwsZooshMSservice();

            service.SendEmail(emailSubject, emailSubject, emailBody, senderName, senderAddress, addressees, attachmentName, null);
        }

        private static string FormatEmailBody(string hsaIdentity, string name, string registerName, string unitName, string templatePathRelative)
        {
            var datetime = DateTime.Now;
            var emailDate = datetime.ToString("yyyy-MM-dd HH:mm");
            var emailTemplate = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(templatePathRelative));
            
            var replacements = new Dictionary<string, string>
            {
                {"{%HSAIdentity%}", hsaIdentity},
                {"{%Name%}", name},
                {"{%RegisterName%}", registerName},
                {"{%UnitName%}", unitName},
                {"{%Date%}", emailDate}
            };

            emailTemplate = replacements.Aggregate(emailTemplate, (current, replacement) => current.Replace(replacement.Key, replacement.Value));

            return emailTemplate;
        }

        public ActionResult LoggGrafikUi(int eventType, int eventNumber, int userId, string eventMessage)
        {
            var logging = new LoggingRepository();
            logging.LoggToDatabase(eventType, eventNumber, userId, eventMessage);

            return null;
        }
    }
}


