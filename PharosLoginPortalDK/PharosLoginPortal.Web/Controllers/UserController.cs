﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;
using Microsoft.AspNet.Identity;
using PharosLoginPortal.Repository.Database;
using PharosLoginPortal.Repository.Helpers;
using PharosLoginPortal.Repository.Models;
using PharosLoginPortal.Web.Helpers;
using PharosLoginPortal.Web.PharosTokenService;

using System.Security.Principal;
using System.Threading;
using System.IdentityModel.Services;
using System.Security.Claims;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace PharosLoginPortal.Web.Controllers
{

    [Authorize]
    public class UserController : Controller
    {
        [HandleError] 
        public ActionResult Index(bool? logout)
        {
            if (logout.HasValue && logout.Value)
            {
                return RedirectToAction("LogOff", "Login");
            }

            var claimsIdentity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
            var hsaIdentity = claimsIdentity.Claims.Where(c => c.Type.EndsWith("CvrNumberIdentifier")).Select(cv => cv.Value).First();

            var indexViewModel = new UserIndexViewModel();

            //if (claimsIdentity.IsAuthenticated)
            //{
            //    var userRepository = new UserRepository();
            //    var registerRepository = new RegisterRepository();

            //    foreach (var register in registerRepository.GetAllRegisters())
            //    {
            //        try
            //        {
            //            var logins = userRepository.GetUsersSithsLogins(register.Id, hsaIdentity);
            //            foreach (var login in logins)
            //            {
            //                login.RegisterName = register.RegisterName;
            //                login.LoginUrl = register.RegisterURL;

            //                var loginRequests = userRepository.GetUsersSithsLoginRequests(register.Id, hsaIdentity).ToArray();

            //                foreach (var request in loginRequests)
            //                {
            //                    request.RegisterName = register.RegisterName;
            //                }

            //                indexViewModel.Logins.AddRange(logins);
            //                indexViewModel.LoginRequests.AddRange(loginRequests);
            //            }
            //        }
            //        catch (Exception e)
            //        {
            //            LogHelper.Error(e.Message, e);
            //        }
            //    }
            //}


            //var user = User.GetAuthifyUser();
            //LogHelper.Debug(string.Format("UserController - Index. HSA-Id: {0}", user.hsa_id));
            var registerRepository = new RegisterRepository();
            var userRepository = new UserRepository();
            //var indexViewModel = new UserIndexViewModel();

            foreach (var register in registerRepository.GetAllRegisters())
            {
                try
                {
                    //var hsaIdentity = user.hsa_id;
                    indexViewModel.HsaId = hsaIdentity;
                    var logins = userRepository.GetUsersSithsLogins(register.Id, hsaIdentity).ToArray();

                    foreach (var login in logins)
                    {
                        login.RegisterName = register.RegisterName;
                        login.LoginUrl = register.RegisterURL;

                        // Admins and Trainers get the admin application
                        if (login.RoleId == 2 || login.RoleId == 5)
                        {
                            login.LoginUrl = register.AdminURL;
                        }

                        if (!login.LoginUrl.EndsWith("/"))
                        {
                            login.LoginUrl += "/";
                        }
                    }

                    //Jämför register Id för att sätta namn på request name.
                    var loginRequests = userRepository.GetUsersSithsLoginRequests(register.Id, hsaIdentity).ToArray();

                    foreach (var request in loginRequests)
                    {
                        request.RegisterName = register.RegisterName;
                    }

                    indexViewModel.Logins.AddRange(logins);
                    indexViewModel.LoginRequests.AddRange(loginRequests);
                }
                catch (Exception e)
                {
                    LogHelper.Error(e.Message, e);
                }
            }
            return View(indexViewModel);
        }

        public RedirectResult LoggGrafikUi(int eventType, int eventNumber, string eventMessage, int userId, string url)
        {
            var user = User.GetAuthifyUser();
            var debug = string.Format("SithToken: {0}, uid: {1}, name: {2}, hsa_id: {3}",
                user.SithToken, user.uid, user.name, user.hsa_id);
            LogHelper.Debug(debug);
            
            var logging = new LoggingRepository();
            logging.LoggToDatabase(eventType, eventNumber, userId, eventMessage);
            
            var service = new TokenWebService();
            var tokenServiceUrl = ConfigHelper.GetTokenWebServiceUrl();
            if (!string.IsNullOrEmpty(tokenServiceUrl))
            {
                service.Url = tokenServiceUrl;
            }

            var token = service.RequestToken(User.GetAuthifyUser().hsa_id);

            LogHelper.Debug("Service returned token: " + token);

            var redirectUrl = string.Format("{0}?pharosToken={1}", url, token);
            return Redirect(redirectUrl);
        }

        public string GetToken(int eventType, int eventNumber, string eventMessage, int userId, int loginId, string url, string unitId)
        {
            var user = User.GetAuthifyUser();
            var doc = new XDocument(new XElement("Credentials",
                                           new XElement("SithsId", user.hsa_id),
                                           new XElement("UserId"),
                                           new XElement("Password"),
                                           new XElement("UnitId", unitId),
                                           new XElement("UnitName"),
                                           new XElement("LoginId", loginId.ToString())));

            LogHelper.Debug("XML:\n" + doc);
            
            var service = new TokenWebService();
            var tokenServiceUrl = ConfigHelper.GetTokenWebServiceUrl();
            if (!string.IsNullOrEmpty(tokenServiceUrl))
            {
                service.Url = tokenServiceUrl;
            }

            var token = service.RequestToken(doc.ToString());
            var redirectUrl = string.Format("{0}Account/LoginFromPortal/?pharosToken={1}", url, token);

            var logging = new LoggingRepository();
            logging.LoggToDatabase(eventType, eventNumber, userId, eventMessage);
            LogHelper.Debug(string.Format("Redirecting user {0} to {1}", user.hsa_id, redirectUrl));

            return redirectUrl;
        }
    }
}

