﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AuthClient_asp;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using NLog.Internal;
using PharosLoginPortal.Repository.Database;
using PharosLoginPortal.Web.Helpers;
using PharosLoginPortal.Web.Models;

namespace PharosLoginPortal.Web.Controllers
{

    [AllowAnonymous]
    public class LoginController : Controller
    {
        private const string ApiKey = "0626b9945469399970d8e008f814945c";
        private const string SecretKey = "c72784f0b1bb66b1042d8d06e8674e82";
        
        public ActionResult Index()
        {
            //var errorlog = new ErrorLoggingRepository();
            //errorlog.LoggToDatabase(); 
            var repository = new RegisterRepository();
            var model = repository.GetAllRegisters();
            return View(model);
        }
        
        public JsonResult RequireLogin(string idp)
        {
            if (Request.Url != null)
            {
                var baseUrl = Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port + HttpContext.Request.ApplicationPath;
                AuthifyClientRest.SetupClient(ApiKey, SecretKey, baseUrl + "/Login/ProcessSithToken");
            }
            var result = AuthifyClientRest.RequireLogin(idp);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoggGrafikUi(int eventType, int eventNumber, int userId, string eventMessage)
        {
            var logging = new LoggingRepository();
            logging.LoggToDatabase(eventType, eventNumber, userId, eventMessage);
            return null;
        }

        // ReSharper disable once InconsistentNaming
        public ActionResult ProcessSithToken(string authify_response_token, int? trainingid)
        {
            try
            {
                LogHelper.Debug("ProcessSithsToken: " + authify_response_token);
                if (trainingid.HasValue) LogHelper.Debug("trainingid: " + trainingid.Value);
                //Hämta användaruppgifter för token från Authyfy
                var userJson = AuthifyClientRest.GetResponse("json", authify_response_token);
                LogHelper.Debug("userJson: " + userJson);

                // Extract the JSON we got from Authify
                var user = JsonConvert.DeserializeObject<AuthifyUserContainer>(userJson).data.Single();
                user.SithToken = authify_response_token;
                user.trainingId = trainingid;
                user.SetupExtraInformationDictionary();
                LogHelper.Debug("user", user);

                // Skapa en session för användaren
                LoginUser(user);

                // Användaren är nu inloggad, skicka användaren till vår förstasida
                return RedirectToAction("Index", "User");
            }
            catch (Exception ex)
            {
                //Om något gick fel
                LogHelper.Error("Error processing SITHS Token", ex);
                return RedirectToAction("AuthenticationError");
            }
        }

        public ActionResult AuthenticationError()
        {
            return View();
        }

#if DEBUG
        public ActionResult FakeLogin()
        {
            LoginUser(AuthifyUser.GetFakeLogin());
            return RedirectToAction("Index", "User");
        }
#endif

        ///<summary>
        ///Use this method to log in the given user
        ///</summary>
        ///<param name="user">The user to be set as logged in</param>
        private void LoginUser(AuthifyUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.name),
                new Claim(ClaimTypes.Email, user.email),
                new Claim(ClaimTypes.UserData, JsonConvert.SerializeObject(user)),
            };

            var claimsIdentity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(claimsIdentity);

            LogHelper.Debug(string.Format("User logged in. HSA-Id: {0}", user.hsa_id));
        }

        /// <summary>
        /// Logs off the currently signed in user, and redirects to login page again
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult LogOff()
        {

        
            
            SessionAuthenticationModule sessionAuthenticationModule = FederatedAuthentication.SessionAuthenticationModule;
            sessionAuthenticationModule?.DeleteSessionTokenCookie();

            FormsAuthentication.SignOut();

            var user = User.GetAuthifyUser();
            LogHelper.Debug(string.Format("User logged out. HSA-Id: {0}", user.hsa_id));
            AuthenticationManager.SignOut();
            //AuthifyClientRest.RequireLogout(user.SithToken);
            Session.Clear();
            Session.Abandon();
            var url = "https://pharosloginportaldk/login";
            WSFederationAuthenticationModule.FederatedSignOut(new Uri(url), new Uri("https://pharosloginportaldk/login/logout.html"));

            return RedirectToAction("Index", "Login");
        }

        /// <summary>
        /// This is a wrapper to handle our user session
        /// </summary>
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}
