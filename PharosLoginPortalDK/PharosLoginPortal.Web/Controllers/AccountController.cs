﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Security.Authentication;
//using System.Security.Claims;
//using System.Web;
//using System.Web.Mvc;
//using Microsoft.AspNet.Identity;
//using Microsoft.Owin.Security;
//using Newtonsoft.Json;
//using PharosLoginPortal.Web.Models;

//namespace PharosLoginPortal.Web.Controllers
//{
//    [Authorize]
//    public class AccountController : Controller
//    {
//        [AllowAnonymous]
//        public ActionResult Login()
//        {
//            ViewBag.Title = "Log In";
//            return View();
//        }

//        [AllowAnonymous]
//        // ReSharper disable once InconsistentNaming
//        public ActionResult ProcessSithToken(string authify_response_token)
//        {
//            try
//            {
//                //Hämta användaruppgifter för token från Authyfy
//                //var userJson = authifyClientRest.GetResponse("json", authify_response_token);
//                var userJson = new AuthifyUserAll.Info.AuthifyUser()
//                {
//                    hsa_id = "SE123456",
//                    firstname = "Richard",
//                    lastname = "Richard Jansson",
//                    email = "richard.jansson@presis.se",

//                };
//                var dummyJson = JsonConvert.SerializeObject(userJson);

//                // Decode the JSON we got from Authify
//                var user = JsonConvert.DeserializeObject<AuthifyUserAll.Info.AuthifyUser>(dummyJson); // use the json from athifyclientRest.getresponse here instead
//                user.SithToken = authify_response_token;

//                if (false) // Kolla att user verkligen verkar vara en korrekt användare (att vi fick vettig data från Authify)
//                    throw new AuthenticationException("You shall not pass!");

//                // Skapa en session för användaren
//                //LoginUser(user);

//                // Användaren är nu inloggad, skicka användaren till vår förstasida
//        //        return RedirectToAction("Index", "Home");
//        //    }
//        //    catch (Exception)
//        //    {
//        //        //Om något gick fel
//        //        return RedirectToAction("AuthenticationError");
//        //    }
//        //}

//        //[AllowAnonymous]
//        //public ActionResult AuthenticationError()
//        //{
//        //    return View();
//        //}

//        /// <summary>
//        /// Use this method to log in the given user
//        /// </summary>
//        /// <param name="user">The user to be set as logged in</param>
//        //private void LoginUser(User user)
//        //{
//        //    var claims = new List<Claim>
//        //    {
//        //        new Claim(ClaimTypes.Name, user.FullName),
//        //        new Claim(ClaimTypes.NameIdentifier, user.Email),
//        //        new Claim(ClaimTypes.PrimarySid, user.Id.ToString("0")),
//        //        new Claim(ClaimTypes.Email, user.Email),
//        //        new Claim(ClaimTypes.Surname, user.LastName),
//        //        new Claim(ClaimTypes.GivenName, user.FirstName),
//        //        new Claim(ClaimTypes.MobilePhone, user.Phone),
//        //        new Claim(ClaimTypes.UserData, JsonConvert.SerializeObject(user)),
//        //    };

//        //    var claimsIdentity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
//        //    AuthenticationManager.SignIn(claimsIdentity);
//        //}


//        /// <summary>
//        /// Logs off the currently signed in user, and redirects to login page again
//        /// </summary>
//        /// <returns></returns>
//        public ActionResult LogOff()
//        {
              //authifyClientRest.RequireLogout((string)Request["authify_response_token"]); 
//            AuthenticationManager.SignOut();
//            return RedirectToAction("Login", "Account");
//        }

//        /// <summary>
//        /// This is a wrapper to handle our user session
//        /// </summary>
//        private IAuthenticationManager AuthenticationManager
//        {
//            get
//            {
//                return HttpContext.GetOwinContext().Authentication;
//            }
//        }
//    }
//}