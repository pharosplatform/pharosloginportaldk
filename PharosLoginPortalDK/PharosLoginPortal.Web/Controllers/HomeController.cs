﻿using System.Linq;
using System.Web.Mvc;
using PharosLoginPortal.Repository.Database;
using PharosLoginPortal.Repository.Models;

namespace PharosLoginPortal.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        /// <summary>
        /// This is the default page to display when the user was logged in successfully
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var registerRepository = new RegisterRepository();
            var userRepository = new UserRepository();
            var indexViewModel = new UserIndexViewModel();

            foreach (var register in registerRepository.GetAllRegisters())
            {
                var logins = userRepository.GetUsersSithsLogins(register.RegisterDatabase).ToArray();

                //RegisterNamn till Logins
                foreach (var login in logins)
                {
                    login.RegisterName = register.RegisterName;
                    login.LoginUrl = register.RegisterURL;

                    //Kolla RoleID för att få
                    if (login.RoleId == 2)
                    {
                        login.LoginUrl = register.AdminURL;
                    }
                    //login.CountyName = 
                }

                //Jämnför register id för att sätta namn på request name.
                var loginRequests = userRepository.GetUsersSithsLoginRequests(register.RegisterDatabase).ToArray();

                foreach (var request in loginRequests)
                {
                    request.RegisterName = register.RegisterName;
                }

                indexViewModel.Logins.AddRange(logins);
                indexViewModel.LoginRequests.AddRange(loginRequests);
            }

            return View(indexViewModel);
        }
    }
}