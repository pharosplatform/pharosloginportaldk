﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using PharosLoginPortal.Repository.Database;
using PharosLoginPortal.Repository.Models;
using PharosLoginPortal.Web.Helpers;

namespace PharosLoginPortal.Web.Controllers
{
    public class WebApiController : ApiController
    {
        /// <summary>
        /// Get the platform level message - shown on the login page page of associated registers
        /// </summary>
        /// <returns>Message string</returns>
        public string GetPlatformMessage()
        {
            var repo = new PlatformMessageRepository();
            return repo.GetPlatformMessage();
        }
        
        public JsonResult<UserIndexViewModel> GetLoginList()
        {
            var registerRepository = new RegisterRepository();
            var userRepository = new UserRepository();
            var indexViewModel = new UserIndexViewModel();

            foreach (var register in registerRepository.GetAllRegisters())
            {
                try
                {
                    var hsaIdentity = User.GetAuthifyUser().hsa_id;
                    indexViewModel.HsaId = hsaIdentity;
                    var logins = userRepository.GetUsersSithsLogins(register.Id, hsaIdentity).ToArray();

                    //RegisterNamn till Logins
                    foreach (var login in logins)
                    {
                        login.RegisterName = register.RegisterName;
                        login.LoginUrl = register.RegisterURL;

                        //Kolla RoleID för att få
                        if (login.RoleId == 2)
                        {
                            login.LoginUrl = register.AdminURL;
                        }
                    }

                    //Jämför register Id för att sätta namn på request name.
                    var loginRequests = userRepository.GetUsersSithsLoginRequests(register.Id, hsaIdentity).ToArray();

                    foreach (var request in loginRequests)
                    {
                        request.RegisterName = register.RegisterName;
                    }

                    indexViewModel.Logins.AddRange(logins);
                    indexViewModel.LoginRequests.AddRange(loginRequests);
                }
                catch (Exception e)
                {
                    LogHelper.Error(e.Message, e);
                }
            }
            return Json(indexViewModel);
        }

    }
}