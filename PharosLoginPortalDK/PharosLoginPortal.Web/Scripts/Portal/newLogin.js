﻿$(document).ready(function () {

    $("#loginInput").hide();
    $("#loginInvalid").hide();
    $("#loginConfirm").hide();
    $("#loginDeny").hide();
    $("#loginDenyAlt").hide();
    $("#wentWrong").hide();
    $("#loginNull").hide();
    $("#emailInput").hide();
    $("#commentInput").hide();
    $("#countyInput").hide();
    $("#communityInput").hide();
    $("#loginQuestion").hide();
    $("#unitInput").hide();
    $("#goodEmail").hide();
    $("#failEmail").hide();
    $("#goodEmail2").hide();
    $("#epicFail").hide();
    $("#failEmail2").hide();
    $("#loginCreated").hide();
    $("#exists").hide();
    $("#forminvalid").hide();
    $("#Email").prop("disabled", true);
    $("#Email2").prop("disabled", true);
    $("#submitBtn").prop("disabled", true);
    var userId;

    //Do not submit on enter
    $(window).keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
        return true;
    });
    
    //Fyll enhetslista vid val av register.
    $("#selectRegister").change(function () {
        //console.log("#selectRegister.val(): " + $(this).val()); // returns "SweTrau"
        //console.log("#selectRegister selected regid): ", $(this).find(":selected").data("regid")); //The Register ID is used instead

        var regid = $(this).find(":selected").data("regid");
        if (regid > -1) {
            // Populate unit dropdown

            //$.getJSON(window.AppRoot + '/NewLogin/GetOrganisationAndUnits/' + $(this).val(), function (data) {
            $.getJSON(window.AppRoot + "NewLogin/GetOrganisationAndUnits/" + regid, function (data) {
                //console.log("*** GetOrganisationAndUnits ***");
                //console.log(data); // returns null
                if (data) {
                    var options = [];
                    options.push("<option value=\"-1\" selected=\"selected\">-- Välj en enhet --</option>");
                    $.each(data, function (index, item) {
                        options.push("<option data-org=" + item.OrganisationId + " value=" + item.UnitId + " >" + item.Name + "</option>");
                    });
                    $("#selectUnit").html(options.join());
                }
            });

            // Populate county dropdown
            //$.getJSON(window.AppRoot + '/NewLogin/GetOrganisationAndUnitsCountyList/' + $(this).val(), function (data) {
            $.getJSON(window.AppRoot + "NewLogin/GetOrganisationAndUnitsCountyList/" + regid, function (data) {
                //console.log("*** GetOrganisationAndUnitsCountyList ***");
                //console.log(data); // returns null
                if (data) {
                    var options = [];
                    options.push("<option value=\"-1\" selected=\"selected\">-- Välj ett Län --</option>");
                    $.each(data, function (index, item) {
                        options.push("<option value=" + item.CountyCode + ">" + item.CountyName + "</option>");
                    });
                    $("#selectCounty").html(options.join());
                }
            });
        }

    });
    
    $("#selectCounty").change(function () {
        //var id = $('#selectRegister').val();
        var regid = $("#selectRegister").find(":selected").data("regid");
        var countyId = $(this).val();
        //console.log("id: " + regid);
        //console.log("countyId: " + countyId);

        // Populate municipality dropdown
        $.getJSON(window.AppRoot + "NewLogin/GetOrganisationsAndUnitsCommunityList/", {
            id: regid,
            countyId: countyId
        }, function (data) {
            //console.log("*** GetOrganisationsAndUnitsCommunityList ***");
            //console.log(data);
            var options = [];
            options.push("<option value=\"-1\" selected=\"selected\">-- Välj en kommun --</option>");
            $.each(data, function (index, item) {
                options.push("<option value=" + item.CommunityCode + ">" + item.CommunityName + "</option>");
            });
            $("#selectCommunity").html(options.join());

        });

    });
    
    $("#selectCommunity").change(function () {
        //var id = $('#selectRegister').val();
        var regid = $("#selectRegister").find(":selected").data("regid");
        var countyId = $("#selectCounty").val();
        var communityId = $(this).val();

        //console.log("id: " + regid);
        //console.log("countyId: " + countyId);
        //console.log("communityId: " + communityId);

        // Populate unit dropdown
        $.getJSON(window.AppRoot + "NewLogin/GetOrganisationAndUnitsOnCountyAndCommunity/", {
            id: regid,
            countyId: countyId,
            communityId: communityId
        }, function (data) {
            //console.log("*** GetOrganisationAndUnitsOnCountyAndCommunity ***");
            //console.log(data);
            var options = [];
            options.push("<option value=\"-1\" selected=\"selected\">-- Välj en enhet --</option>");
            $.each(data, function (index, item) {
                options.push("<option data-org=" + item.OrganisationId + " value=" + item.UnitId + " >" + item.Name + "</option>");

            });
            $("#selectUnit").html(options.join());

        });

    });
    
    //Kollar om användaren har ett personligt login sedan tidigare (Stored procedure)
    $("#checkLoginbtn").click(function () {
        var password = $("#Password").val();
        var username = $("#Username").val();
        //var database = $('#selectRegister').val();
        var regid = $("#selectRegister").find(":selected").data("regid");
        var unitId = $("#selectUnit").val();

        if (!username || !password) {
            $("#loginInvalid").slideDown();
            setTimeout(function () {
                $('#loginInvalid').slideUp();
            }, 5000);
            return;
        }

        $.getJSON(window.AppRoot + "NewLogin/DoesPersonalLoginExist/", {
            registerId: regid,
            password: password,
            username: username,
            unitId: unitId
        }, function (result) {
            var answer = result;
            console.log('DoesPersonalLoginExist answer', answer);

            if (!answer) {
                $("#wentWrong").slideDown();
                setTimeout(function () {
                    $('#wentWrong').slideUp();
                }, 10000);

            } else {
                if (answer.ReturnValue === 1) {
                    $("#loginConfirm").slideDown();
                    setTimeout(function () {
                        $('#loginConfirm').slideUp();
                    }, 6000);

                    // Registrerar i Eventlog-databasen
                    var eventMessage = "User Personal login exists";
                    var eventType = 1;
                    var eventNumber = 33;

                    userId = answer.UserId;

                    $("#loginInput input, #loginInput button").prop("disabled", true);

                    $("#submitBtn").prop("disabled", false);

                    $.getJSON(window.AppRoot + "Login/LoggGrafikUi/", {
                        eventType: eventType,
                        userId: userId,
                        eventNumber: eventNumber,
                        eventMessage: eventMessage
                    });
                    //
                } else if (answer.ReturnValue === 0) {
                    var msgId = $("#forcePersonalLogin").val() === "True" ? "#loginDenyAlt" : "#loginDeny";
                    $(msgId).slideDown();
                    setTimeout(function () {
                        $(msgId).slideUp();
                    }, 10000);

                    // Registrerar i Eventlog-databasen
                    var eventMessage2 = "User Personal login doesn't exist/wrong";
                    var eventType2 = 2;
                    var eventNumber2 = 34;

                    $.getJSON(window.AppRoot + "Login/LoggGrafikUi/", {
                        eventType: eventType2,
                        userId: userId,
                        eventNumber: eventNumber2,
                        eventMessage: eventMessage2
                    });

                } else if (answer === 5) {
                    $("#epicFail").slideDown();
                } else {
                    $("#epicFail").slideDown();

                    // Registrerar i Eventlog-databasen
                    var eventMessage3 = "Error checking user. Answer = " + answer;
                    var eventType3 = 2;
                    var eventNumber3 = 35;

                    $.getJSON(window.AppRoot + "Login/LoggGrafikUi/", {
                        eventType: eventType3,
                        userId: userId,
                        eventNumber: eventNumber3,
                        eventMessage: eventMessage3
                    });
                }
            }
        });
    });

    $("#submitBtn").click(function (evt) {
        evt.preventDefault();

        var database = $("#selectRegister").val();
        var email = $("#Email").val();
        var userComment = $("#userComment").val();
        var registerId = $("#selectRegister").find("option:selected").data("regid");
        var organisationId = $("#selectUnit").find("option:selected").data("org");
        var unitId = $("#selectUnit").val();
        var loginRequestId = 0;

        if (email === "" || unitId < 0) {
            $("#forminvalid").slideDown();
            setTimeout(function () {
                $('#forminvalid').slideUp();
            }, 10000);
            return;
        }

        $.getJSON(window.AppRoot + "NewLogin/CreateSithsLoginRequest/", {
            database: database,
            email: email,
            userComment: userComment,
            userId: userId,
            registerId: registerId,
            organisationId: organisationId,
            unitId: unitId,
            loginRequestId: loginRequestId
        }, function (result) {
            var answer = result;
            //console.log('CreateSithsLoginRequest answer', answer);
            if (answer !== -5) {
                if (answer === "Siths login finns redan") {
                    $("#exists").slideDown().text(answer);
                    setTimeout(function() {
                        $('#exists').slideUp();
                    }, 10000);

                } else if (answer === "Ansökan finns redan") {
                    $("#exists").slideDown().text(answer);
                    setTimeout(function () {
                        $('#exists').slideUp();
                    }, 10000);
                } else {
                    //Redirect to list
                    $("#loginCreated").slideDown();
                    setTimeout(function () {
                        location.href = window.AppRoot;
                    }, 3000);
                }
            } else {
                $("#epicFail").slideDown();
            }
        });

    });

    function showRestOfForm() {
        $("#emailInput").slideDown();
        $("#commentInput").slideDown();

        if ($("#forcePersonalLogin").val() === "True") {
            $("#userQInput").val("1");
            $("#loginQuestion").hide();

            $("#loginInput").slideDown();
            $("#submitBtn").prop("disabled", true);
        } else {
            $("#loginQuestion").slideDown();
            $("#submitBtn").prop("disabled", false);
        }
    }

    function hideRestofForm() {
        $("#emailInput").slideUp();
        $("#commentInput").slideUp();
        $("#loginQuestion").slideUp();
        $("#userQInput").val("0");
        $("#loginInput").slideUp();
        $("#submitBtn").prop("disabled", true);
    }

    // Gömmer personlig inloggning om användaren säger att de inte har ett. 
    $("#userQInput").change(function () {
        //console.log("#userQInput change", $(this).val());
        if ($(this).val() === "1") {
            $("#loginInput").slideDown();
            $("#loginInput input, #loginInput button").prop("disabled", false);
            $("#submitBtn").prop("disabled", true);
        } else {
            $("#loginInput").slideUp();
            $("#submitBtn").prop("disabled", false);
        }
    });

    // När du valt register så syns enhet
    $("#selectRegister").change(function () {
        //console.log($("#selectRegister").val());
        if ($("#selectRegister").val() !== "-1") {
            var unitSelectionType = $("#selectRegister").find("option:selected").data("selectpe");

            var forcePersonalLogin = $("#selectRegister").find("option:selected").data("force-personal-login");
            $("#forcePersonalLogin").val(forcePersonalLogin);

            //$("#emailInput").hide();
            //$("#commentInput").hide();
            //$("#loginQuestion").hide();
            //$("#submitBtn").prop("disabled", true);

            //console.log(unitSelectionType);
            if (unitSelectionType === 1) {
                $("#unitInput").slideDown();
                $("#countyInput").slideUp();
                $("#communityInput").slideUp();
            } else {
                $("#countyInput").slideDown();
                $("#communityInput").slideUp();
                $("#unitInput").slideUp();
            }
            hideRestofForm();
        } else {
            $("#countyInput").slideUp();
            $("#communityInput").slideUp();
            $("#unitInput").slideUp();
            hideRestofForm();
        }

    });

    $("#selectCounty").change(function () {
        if ($("#selectCounty").val() > -1) {
            $("#communityInput").slideDown();
        } else {
            $("#communityInput").slideUp();
        }
        $("#unitInput").slideUp();
        hideRestofForm();
    });

    $("#selectCommunity").change(function () {
        //console.log("#selectCommunity", $("#selectCommunity").val());
        if ($("#selectCommunity").val() > 0) {
            $("#unitInput").slideDown();
        } else {
            $("#unitInput").slideUp();
            hideRestofForm();
        }
    });

    //När du valt enhet syns email input.
    $("#selectUnit").change(function () {
        if ($("#selectUnit").val() > -1) {
            showRestOfForm();
        } else {
            hideRestofForm();
        }
    });

    $("#selectUnit").change(function () {
        var eventMessage = "Selected unit: " + $("#selectUnit option:selected").text();
        var eventType = 1;
        var eventNumber = 31;


        $.getJSON(window.AppRoot + "Login/LoggGrafikUi/", {
            eventType: eventType,
            userId: userId,
            eventNumber: eventNumber,
            eventMessage: eventMessage
        });
    });

    $("#changeEmail").click(function () {
        $("#Email").prop("disabled", false);
        $("#Email2").prop("disabled", false);
    });

    //När Email2 validerats så ser loginQuestion
    $("#Email2").on("input", function () {
        var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (emailReg.test($(this).val()) && $("#Email").val() === $("#Email2").val() && $("#Email").val() != null) {

        }
    });

    //När Email validerats visar en positiv bock eller negativt kors
    $("#Email").on("input", function () {
        var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (emailReg.test($(this).val())) {
            $("#goodEmail").show();
            $("#failEmail").hide();

        } else {
            $("#failEmail").show();
            $("#goodEmail").hide();
            $("#submitBtn").prop("disabled", true);
        }
    });

    //När Email2 validerats visar en positiv bock eller negativt kors
    $("#Email2").change(function () {
        var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (emailReg.test($(this).val()) && $("#Email2").val() === $("#Email").val()) {
            $("#goodEmail2").show();
            $("#failEmail2").hide();
            if ($("#forcePersonalLogin").val() !== "True" || userId > 0) {
                //Only enable submit-button if not forced to login or userId is set
                $("#submitBtn").prop("disabled", false);
            }
        } else {
            $("#failEmail2").show();
            $("#goodEmail2").hide();
            $("#submitBtn").prop("disabled", true);
        }
    });
    
    $("#cancelBtn").click(function (evt) {
        evt.preventDefault();
        location.href = window.AppRoot;
    });

});



