﻿
$(document).ready(function () {
    //if (sessionStorage.token === null || sessionStorage.token === undefined || sessionStorage.token === "null") {
    //    window.location.replace("../Login");
    //}

    //Disables input on load
    $("input").attr('disabled', 'disabled');

    //Enables onClick
    $("#editButton").click(function() {
        $("input").removeAttr('disabled');
    });

    //Disables onClick
    $("#saveButton").click(function () {
        $("input").attr('disabled', 'disabled');
        $("#confirmEmail").slideUp();
    });
    

    //Hides confirm e-mail inputfield
    $("#confirmEmail").hide();

    //On focus e-mail confirm-field
    $("#editUserEmail").focus(function () {

        $("#confirmEmail").slideDown();
    });

    $('.clickableRow').click(function () {
        var eventMessage = "Clicked register in table: " + $(this).data("name");
        var eventType = 1;
        var eventNumber = 32;

        var userId = $(this).data("userid");
        var loginId = $(this).data("loginid");
        var url = $(this).data("url");
        var unitId = $(this).data("unitid");

        $("#navbar").addClass("working");

        $.get(window.AppRoot + "User/GetToken", {
            eventType: eventType,
            eventNumber: eventNumber,
            eventMessage: eventMessage,
            userId: userId,
            loginId: loginId,
            url: url,
            unitId: unitId
        }, function (tokenizedURL) {
            window.location = tokenizedURL;
        });

    });
});


