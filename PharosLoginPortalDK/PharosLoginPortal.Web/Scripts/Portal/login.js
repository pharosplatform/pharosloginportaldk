﻿//Disable ajax cache globally
$.ajaxSetup({ cache: false });


$(document).ready(function () {

    $("#loginBox").hide();

    $(".navbar-toggle").on("click", function () {
        $(".after-navbar").css("margin-top", "230px");
    });

    $("#personalLoginButton, #personalLoginLink").click(function () {
        $("#loginBox").slideToggle();
    });

    $("#dropDownRegisters").change(function () {
        var eventMessage = "Personal instead of Sith redirected to register: " + $("#dropDownRegisters option:selected").text();
        var eventType = 1;
        var eventNumber = 35;
        var userId = 0;

        $.getJSON(window.AppRoot + "/Login/LoggGrafikUi/", {
            eventType: eventType,
            userId: userId,
            eventNumber: eventNumber,
            eventMessage: eventMessage
        });
    });

    $("#personalLoginAbort").click(function () {
        $("#loginBox").slideToggle();
    });

    $('#nemIDLoginButton').click(function() {
        $("#navbar").addClass("working");
        
        window.location.replace("https://pharosloginportaldk/");
    });

    $("#sithsLoginButton, #sithsLoginLink").click(function () {
        var idp = "siths";
        $("#navbar").addClass("working");

        $.getJSON(window.AppRoot + "Login/RequireLogin/", {
            idp: idp
        }, function (result) {
            var answer = result;
            window.location.replace(answer);
            console.log("AJAX login request returns: " + answer);
            console.log("sessionStorage.token: " + sessionStorage.token);
        });
    });

    //Get platform message
    $.get(window.AppRoot + 'api/WebApi/GetPlatformMessage', function(data) {
        if (data) {
            $("#platformMessage").html(data.replace(/\n/g, "<br />"));
            $("#platformMessage").parent().fadeIn(500);
        }
    });
});





