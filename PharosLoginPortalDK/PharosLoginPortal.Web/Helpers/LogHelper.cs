﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLog;
using Newtonsoft.Json;

namespace PharosLoginPortal.Web.Helpers
{
    public static class LogHelper
    {

        
        public static Logger Instance { get; private set; }

        static LogHelper()
        {
            Instance = LogManager.GetCurrentClassLogger();
        }

        public static void Debug(string logMessage)
        {
            Instance.Debug(logMessage);
        }

        public static void Debug(string logMessage, object obj)
        {
            Instance.Debug(logMessage);
            Instance.Debug(obj.ToJson());
        }

        public static void Info(string logMessage)
        {
            Instance.Info(logMessage);
        }

        public static void Warning(string logMessage)
        {
            Instance.Warn(logMessage);
        }

        public static void Error(string logMessage)
        {
            Instance.Error(logMessage);
        }

        public static void Error(string logMessage, Exception exc)
        {
            Instance.ErrorException(logMessage, exc);
        }

        private static string ToJson(this object value)
        {
            var settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            return JsonConvert.SerializeObject(value, Formatting.Indented, settings);
        }
    }

}