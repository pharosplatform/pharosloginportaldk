﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace PharosLoginPortal.Web.Npat.Eyenet.EmailService {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.79.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="IwsZooshMSbinding", Namespace="http://tempuri.org/")]
    public partial class IwsZooshMSservice : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback SendEmailOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public IwsZooshMSservice() {
            this.Url = global::PharosLoginPortal.Web.Properties.Settings.Default.PharosLoginPortal_Web_Npat_Eyenet_EmailService_IwsZooshMSservice;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event SendEmailCompletedEventHandler SendEmailCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("urn:uwsZooshMSIntf-IwsZooshMS#SendEmail", RequestNamespace="urn:uwsZooshMSIntf-IwsZooshMS", ResponseNamespace="urn:uwsZooshMSIntf-IwsZooshMS")]
        [return: System.Xml.Serialization.SoapElementAttribute("return")]
        public bool SendEmail(string Description, string EmailSubject, string EmailBody, string SenderName, string SenderAddress, string Addressees, string AttachmentName, [System.Xml.Serialization.SoapElementAttribute(DataType="base64Binary")] byte[] AttachmentFile) {
            object[] results = this.Invoke("SendEmail", new object[] {
                        Description,
                        EmailSubject,
                        EmailBody,
                        SenderName,
                        SenderAddress,
                        Addressees,
                        AttachmentName,
                        AttachmentFile});
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void SendEmailAsync(string Description, string EmailSubject, string EmailBody, string SenderName, string SenderAddress, string Addressees, string AttachmentName, byte[] AttachmentFile) {
            this.SendEmailAsync(Description, EmailSubject, EmailBody, SenderName, SenderAddress, Addressees, AttachmentName, AttachmentFile, null);
        }
        
        /// <remarks/>
        public void SendEmailAsync(string Description, string EmailSubject, string EmailBody, string SenderName, string SenderAddress, string Addressees, string AttachmentName, byte[] AttachmentFile, object userState) {
            if ((this.SendEmailOperationCompleted == null)) {
                this.SendEmailOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSendEmailOperationCompleted);
            }
            this.InvokeAsync("SendEmail", new object[] {
                        Description,
                        EmailSubject,
                        EmailBody,
                        SenderName,
                        SenderAddress,
                        Addressees,
                        AttachmentName,
                        AttachmentFile}, this.SendEmailOperationCompleted, userState);
        }
        
        private void OnSendEmailOperationCompleted(object arg) {
            if ((this.SendEmailCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SendEmailCompleted(this, new SendEmailCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.79.0")]
    public delegate void SendEmailCompletedEventHandler(object sender, SendEmailCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.79.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class SendEmailCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal SendEmailCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591